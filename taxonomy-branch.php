<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * The template for displaying Archive Pages
 */

$us_layout = US_Layout::instance();

get_header();

// Output Title Bar
$titlebar_vars = array();
if ( is_category() OR is_tag() OR is_tax() ) {
	$term = get_queried_object();
	if ( $term ) {
		$taxonomy = $term->taxonomy;
		$term = $term->term_id;
	}
	$titlebar_vars['subtitle'] = nl2br( get_term_field( 'description', $term, $taxonomy, 'edit' ) );
}
us_load_template( 'templates/titlebar', $titlebar_vars );

?>
<div class="l-main">
	<div class="l-main-h i-cf">

		<main class="l-content"<?php echo ( us_get_option( 'schema_markup' ) ) ? ' itemprop="mainContentOfPage"' : ''; ?>>
            <section class="l-section-h i-cf">
                <?php echo do_shortcode(term_description()); ?>
                <div class="row meet_out_team">
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="col-12 col-lg-4 member">
                            <?php the_post_thumbnail(); ?>
                            <a class="title" href="<?php the_permalink(); ?>">
                                <span class="name"><?php the_title(); ?></span>
                                <?php the_field('position'); ?>
                            </a>
                        </div>
                    <?php endwhile; ?>
                </div>
            </section>
		</main>

		<?php us_load_template( 'templates/sidebar' ) ?>

	</div>
</div>

<?php get_footer() ?>
