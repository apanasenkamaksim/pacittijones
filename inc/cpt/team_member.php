<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 9/10/2019
 * Time: 6:57 PM
 */

function cptui_register_my_cpts_team_member() {

    /**
     * Post Type: Team members.
     */

    $labels = array(
        "name" => __( "Team members", "custom-post-type-ui" ),
        "singular_name" => __( "Team member", "custom-post-type-ui" ),
    );

    $args = array(
        "label" => __( "Team members", "custom-post-type-ui" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "team_member", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "team_member", $args );
}

add_action( 'init', 'cptui_register_my_cpts_team_member' );


function cptui_register_my_taxes_branch() {

    /**
     * Taxonomy: Branches.
     */

    $labels = array(
        "name" => __( "Branches", "custom-post-type-ui" ),
        "singular_name" => __( "Branch", "custom-post-type-ui" ),
    );

    $args = array(
        "label" => __( "Branches", "custom-post-type-ui" ),
        "labels" => $labels,
        "public" => true,
        "publicly_queryable" => true,
        "hierarchical" => true,
        "show_ui" => true,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "query_var" => true,
        "rewrite" => array( 'slug' => 'branch', 'with_front' => true, ),
        "show_admin_column" => false,
        "show_in_rest" => true,
        "rest_base" => "branch",
        "rest_controller_class" => "WP_REST_Terms_Controller",
        "show_in_quick_edit" => false,
    );
    register_taxonomy( "branch", array( "team_member" ), $args );
}
add_action( 'init', 'cptui_register_my_taxes_branch' );
