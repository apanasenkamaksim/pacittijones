<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 9/13/2019
 * Time: 3:21 PM
 */

function cptui_register_my_cpts_charity() {

    /**
     * Post Type: Posts.
     */

    $labels = array(
        "name" => __( "Posts", "custom-post-type-ui" ),
        "singular_name" => __( "Post", "custom-post-type-ui" ),
        "menu_name" => __( "Сharity", "custom-post-type-ui" ),
    );

    $args = array(
        "label" => __( "Posts", "custom-post-type-ui" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => false,
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "charity", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "charity", $args );
}

add_action( 'init', 'cptui_register_my_cpts_charity' );
