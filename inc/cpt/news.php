<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 7/15/2019
 * Time: 9:30 AM
 */

function cptui_register_my_cpts_news() {

    /**
     * Post Type: News.
     */

    $labels = array(
        "name" => __( "News", "custom-post-type-ui" ),
        "singular_name" => __( "New", "custom-post-type-ui" ),
    );

    $args = array(
        "label" => __( "News", "custom-post-type-ui" ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "delete_with_user" => false,
        "show_in_rest" => true,
        "rest_base" => "",
        "rest_controller_class" => "WP_REST_Posts_Controller",
        "has_archive" => "news",
        "show_in_menu" => true,
        "show_in_nav_menus" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "news", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "thumbnail" ),
    );

    register_post_type( "news", $args );
}

add_action( 'init', 'cptui_register_my_cpts_news' );
