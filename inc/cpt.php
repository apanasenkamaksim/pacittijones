<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 7/15/2019
 * Time: 9:30 AM
 */

include_once __DIR__ . '/cpt/news.php';
include_once __DIR__ . '/cpt/team_member.php';
include_once __DIR__ . '/cpt/charity.php';