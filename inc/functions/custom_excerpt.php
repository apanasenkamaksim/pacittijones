<?php
// Function to cut the string
function cut_string($string, $count, $addition=''){
  if(mb_strlen($string, 'utf-8')<=$count){
	  return $string;
  }else{
	  $new_string=mb_substr($string, 0, $count,'utf-8');

	  if (preg_match("/^(_|\W)$/iu", substr($new_string, -1))) {
          $new_string=preg_replace("/(_|\W)+$/iu", "", $new_string);
	  } else {
          $new_string=preg_replace("/(_|\W)+\w+$/iu", "", $new_string);
	  }

	  return $new_string . $addition;
  }
}

// Function returns cut string
function return_excerpt($count, $addition = false){
	global $post;
	$excerpt = get_the_content();
	$excerpt = preg_replace('#<style[^>]*?>.*?</style>#si', '', $excerpt);
	$excerpt = preg_replace('#<script[^>]*?>.*?</script>#si', '', $excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	if(!$addition) $addition = '... <a href="' . get_permalink() . '">' . __('more', 'imedix') . '</a>';
	$excerpt = cut_string($excerpt, $count, $addition);

	return $excerpt;
}

// Function to echo cut string
function echo_excerpt($count, $addition){
	echo return_excerpt($count, $addition);
}
