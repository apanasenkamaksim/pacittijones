<?php
/**
 * Created by seattleby.com
 * Date: 12.12.2018
 * Time: 15:04
 */

function custom_scripts_styles()
{
    $cache_buster = date("YmdHi", filemtime( get_stylesheet_directory_uri() . '/production/css/app.css'));
    wp_register_style('theme-style', get_stylesheet_directory_uri() . '/production/css/app.css', array(), $cache_buster, 'all');
    wp_enqueue_style('theme-style');

    $cache_buster = date("YmdHi", filemtime( get_stylesheet_directory_uri() . '/production/js/app.js'));
    wp_register_script('theme-script', get_stylesheet_directory_uri() . '/production/js/app.js', array('jquery'), $cache_buster, true);
    wp_localize_script('theme-script', 'theme', array(
        'url' => home_url(),
        'themeUrl' => get_stylesheet_directory_uri(),
        'ajaxUrl' => admin_url('admin-ajax.php')
    ));
    wp_enqueue_script('theme-script');

    global $posts;

    if(is_singular('property')) {
        register_new_royalslider_files(3);
    }
}

add_action('wp_enqueue_scripts', 'custom_scripts_styles');