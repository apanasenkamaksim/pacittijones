<?php defined('ABSPATH') OR die('This script cannot be accessed directly.');

/**
 * Template to show single page or any post type
 */

$us_layout = US_Layout::instance();

get_header();

global $us_iframe;
if(!$us_iframe) {
    us_load_template('templates/titlebar');
}

?>
<div class="l-main">
    <div class="l-main-h i-cf">

        <main class="l-content"<?php echo (us_get_option('schema_markup')) ? ' itemprop="mainContentOfPage"' : ''; ?>>
            <section class="l-section-h i-cf">
                <div class="row meet_out_team member">
                    <?php do_action('us_before_page');

                    while (have_posts()) {
                        the_post();

                        $image1 = get_field('image_1');
                        $image2 = get_field('image_2');
                        ?>

                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-lg-4 mb-5 mb-lg-0">
                                    <div class="image"
                                         style="background-image: url('<?php echo $image1['sizes']['medium']; ?>');"></div>
                                </div>
                                <div class="col-12 col-lg-4">
                                    <div class="image"
                                         style="background-image: url('<?php echo $image2['sizes']['medium']; ?>');"></div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="row">
                                <div class="col-12 col-lg-8">
                                    <h2 class="title">
                                        <div class="name">
                                            <?php the_title(); ?>
                                        </div>
                                        <?php the_field('position'); ?>
                                    </h2>
                                    <div class="description">
                                        <?php the_content(); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php
                    }
                    do_action('us_after_page');
                    ?>
                </div>
            </section>
        </main>

        <?php us_load_template('templates/sidebar') ?>

    </div>
</div>

<?php get_footer() ?>
