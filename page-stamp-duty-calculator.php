<?php defined('ABSPATH') OR die('This script cannot be accessed directly.');

/**
 * Template to show single page or any post type
 */

$us_layout = US_Layout::instance();

get_header();

global $us_iframe;
if(!$us_iframe) {
    us_load_template('templates/titlebar');
}

?>
<div class="l-main">
    <div class="l-main-h i-cf">

        <main class="l-content"<?php echo (us_get_option('schema_markup')) ? ' itemprop="mainContentOfPage"' : ''; ?>>
            <section class="l-section-h i-cf">
                <?php do_action('us_before_page'); ?>
                <div class="row stamp-duty-calculator">
                    <div class="col-12">
                        <h2 class="h1 font-weight-light mb-4">
                            Land and Buildings Transaction Tax (LBTT) calculator
                        </h2>

                        <?php function get_calculator($c) { ?>
                            <?php if($c == 'Scotland') : ?>
                                <h3 class="h2 font-weight-light mb-4">
                                    Calculate the Land and Buildings Transaction Tax on your residential property in
                                    Scotland
                                </h3>
                                <div class="text">
                                    Land and Buildings Transaction Tax (LBTT) has replaced Stamp Duty in Scotland. If
                                    you’re buying a residential property over £145,000, you’ll have to pay LBTT. This
                                    calculator works out how much Land and Buildings Transaction Tax you need to pay on
                                    your new home. It also works out how much you’ll pay on any additional property
                                    purchase over £40,000, like a buy to let or second home, which attracts an extra 4%
                                    Additional Dwelling Supplement (ADS).
                                </div>
                            <?php elseif($c == 'England') : ?>
                                <h3 class="h2 font-weight-light mb-4">
                                    Calculate the Stamp Duty on your residential property purchase in England or
                                    Northern Ireland
                                </h3>
                                <div class="text">
                                    Stamp Duty Land Tax (SDLT) is a tax on properties bought in England and Northern
                                    Ireland. You’ll need to pay it when you buy a residential property that costs more
                                    than £125,000. Use this calculator to work out how much Stamp Duty you’ll need to
                                    pay on your new home. You can also use it to work out how much you’ll pay on an
                                    additional property that costs more than £40,000, like a buy to let or second home,
                                    which attracts an extra 3% charge.
                                </div>
                            <?php elseif($c == 'Wales') : ?>
                                <h3 class="h2 font-weight-light mb-4">
                                    Calculate the Land Transaction Tax on your residential property in Wales
                                </h3>
                                <div class="text">
                                    Stamp Duty has been replaced by Land Transaction Tax (LTT) for properties bought in
                                    Wales. LTT will apply to residential properties costing over £180,000. You can use
                                    this calculator to find the amount of Land Transaction Tax you’ll have to pay. You
                                    can also work out the cost of LTT on any additional properties you purchase over
                                    £40,000, like buy to lets or second homes, which will be charged a 3% additional
                                    supplement.
                                </div>
                            <?php endif; ?>
                            <form action="">
                                <div class="row align-items-center">
                                    <div class="col-12 col-md-auto">
                                        I am
                                    </div>
                                    <div class="col">
                                        <select name="type">
                                            <option value="">
                                                please select an option
                                            </option>
                                            <option value="bmnh" <?php if(!empty($_REQUEST['type']) && $_REQUEST['type'] == 'bmnh')
                                                echo 'selected="selected"'; ?>>
                                                buying my next home
                                            </option>
                                            <option value="ftb" <?php if(!empty($_REQUEST['type']) && $_REQUEST['type'] == 'ftb')
                                                echo 'selected="selected"'; ?>>
                                                a first time buyer
                                            </option>
                                            <option value="btl" <?php if(!empty($_REQUEST['type']) && $_REQUEST['type'] == 'btl')
                                                echo 'selected="selected"'; ?>>
                                                buying an additional or buy-to-let property
                                            </option>
                                        </select>
                                    </div>
                                    <div class="col-auto">
                                        <div class="info-block">
                                            <i class="far fa-info-circle info-icon"></i>
                                            <div class="info">
                                                <i class="far fa-times-circle info-close"></i>
                                                Anyone purchasing an additional home including buy to let properties
                                                over the value of £40,000 will have to pay an extra 4% surcharge on top
                                                of each Land and Buildings Transaction Tax band
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-12 col-md-auto">
                                        Price property
                                    </div>
                                    <div class="col-12 col-md">
                                        <input type="number" name="price" placeholder="£"
                                               value="<?php if(!empty($_REQUEST['price']))
                                                   echo $_REQUEST['price']; ?>">
                                    </div>
                                    <div class="col-12 col-md-auto">
                                        <input type="hidden" name="country" value="<?php echo $c; ?>">
                                        <input type="submit" value="Next">
                                    </div>
                                </div>
                            </form>
                        <?php } ?>

                        <?php
                        $show_form = empty($_REQUEST['type']) || empty($_REQUEST['country']) || empty($_REQUEST['price']);
                        if($show_form) {
                            ob_start();
                            get_calculator('Scotland');
                            $tab1 = ob_get_clean();

                            ob_start();
                            get_calculator('England');
                            $tab2 = ob_get_clean();

                            ob_start();
                            get_calculator('Wales');
                            $tab3 = ob_get_clean();

                            $tab1_active = !empty($_REQUEST['country']) && $_REQUEST['country'] == 'Scotland' ? ' active="1"' : '';
                            $tab2_active = !empty($_REQUEST['country']) && $_REQUEST['country'] == 'England' ? ' active="1"' : '';
                            $tab3_active = !empty($_REQUEST['country']) && $_REQUEST['country'] == 'Wales' ? ' active="1"' : '';

                            ?>

                            <div class="screen-1">
                                <?php
                                echo do_shortcode('[vc_tta_tabs title_font="body"]
                                    [vc_tta_section title="Scotland" tab_id="one"' . $tab1_active . ']' . $tab1 . '[/vc_tta_section]
                                    [vc_tta_section title="England & NI" tab_id="two"' . $tab2_active . ']' . $tab2 . '[/vc_tta_section]
                                    [vc_tta_section title="Wales" tab_id="three"' . $tab3_active . ']' . $tab3 . '[/vc_tta_section]
                                    [/vc_tta_tabs]');
                                ?>
                            </div>
                            <?php
                        }
                        ?>
                        <?php if(!$show_form) : ?>
                            <?php
                            $scotland  = array(array('from' => 0,
                                                     'to'   => 40000,
                                                     'ftb'  => 0,
                                                     'bmnh' => 0,
                                                     'btl'  => 0
                                               ),
                                               array('from' => 40000,
                                                     'to'   => 145000,
                                                     'ftb'  => 0,
                                                     'bmnh' => 0,
                                                     'btl'  => 0.04
                                               ),
                                               array('from' => 145000,
                                                     'to'   => 175000,
                                                     'ftb'  => 0,
                                                     'bmnh' => 0.02,
                                                     'btl'  => 0.06
                                               ),
                                               array('from' => 175000,
                                                     'to'   => 250000,
                                                     'ftb'  => 0.02,
                                                     'bmnh' => 0.02,
                                                     'btl'  => 0.06
                                               ),
                                               array('from' => 250000,
                                                     'to'   => 325000,
                                                     'ftb'  => 0.05,
                                                     'bmnh' => 0.05,
                                                     'btl'  => 0.09
                                               ),
                                               array('from' => 325000,
                                                     'to'   => 750000,
                                                     'ftb'  => 0.1,
                                                     'bmnh' => 0.1,
                                                     'btl'  => 0.14
                                               ),
                                               array('from' => 750000,
                                                     'ftb'  => 0.12,
                                                     'bmnh' => 0.12,
                                                     'btl'  => 0.16
                                               )
                            );
                            $wales     = array(array('from' => 0,
                                                     'to'   => 180000,
                                                     'ftb'  => 0,
                                                     'bmnh' => 0,
                                                     'btl'  => 0.03
                                               ),
                                               array('from' => 180000,
                                                     'to'   => 250000,
                                                     'ftb'  => 0.035,
                                                     'bmnh' => 0.035,
                                                     'btl'  => 0.065
                                               ),
                                               array('from' => 250000,
                                                     'to'   => 400000,
                                                     'ftb'  => 0.05,
                                                     'bmnh' => 0.05,
                                                     'btl'  => 0.08
                                               ),
                                               array('from' => 400000,
                                                     'to'   => 750000,
                                                     'ftb'  => 0.075,
                                                     'bmnh' => 0.075,
                                                     'btl'  => 0.105
                                               ),
                                               array('from' => 750000,
                                                     'to'   => 1500000,
                                                     'ftb'  => 0.1,
                                                     'bmnh' => 0.1,
                                                     'btl'  => 0.13
                                               ),
                                               array('from' => 1500000,
                                                     'ftb'  => 0.12,
                                                     'bmnh' => 0.12,
                                                     'btl'  => 0.15
                                               )
                            );
                            $england   = array(array('from' => 0,
                                                     'to'   => 125000,
                                                     'ftb'  => 0,
                                                     'bmnh' => 0,
                                                     'btl'  => 0.03
                                               ),
                                               array('from' => 125000,
                                                     'to'   => 250000,
                                                     'ftb'  => 0,
                                                     'bmnh' => 0.02,
                                                     'btl'  => 0.05
                                               ),
                                               array('from' => 250000,
                                                     'to'   => 300000,
                                                     'ftb'  => 0,
                                                     'bmnh' => 0.05,
                                                     'btl'  => 0.08
                                               ),
                                               array('from' => 300000,
                                                     'to'   => 500000,
                                                     'ftb'  => 0.05,
                                                     'bmnh' => 0.05,
                                                     'btl'  => 0.08
                                               ),
                                               array('from' => 500000,
                                                     'to'   => 925000,
                                                     'ftb'  => 0.05,
                                                     'bmnh' => 0.05,
                                                     'btl'  => 0.08
                                               ),
                                               array('from' => 925000,
                                                     'to'   => 1500000,
                                                     'ftb'  => 0.1,
                                                     'bmnh' => 0.1,
                                                     'btl'  => 0.13
                                               ),
                                               array('from' => 1500000,
                                                     'ftb'  => 0.12,
                                                     'bmnh' => 0.12,
                                                     'btl'  => 0.15
                                               )
                            );
                            $rates     = ${mb_strtolower($_REQUEST['country'])};
                            $rate_type = $_REQUEST['type'] == 'btl' ? 'bmnh' : $_REQUEST['type'];
                            $price     = $_REQUEST['price'];
                            $count     = count($rates);
                            $i         = 1;
                            foreach ($rates as $key => $rate) {
                                if(($count != $i && $rate['from'] <= $price && $price < $rate['to']) || ($count == $i && $rate['from'] <= $price)) {
                                    $additional_price = ($price - $rate['from']) * $rate[$rate_type];
                                    for ($j = $i - 2; $j >= 0; $j--) {
                                        $additional_price += ($rates[$j]['to'] - $rates[$j]['from']) * $rates[$j][$rate_type];
                                    }

                                    break;
                                }
                                $i++;
                            }
                            if($_REQUEST['type'] == 'btl') {
                                $i = 1;
                                foreach ($rates as $key => $rate) {
                                    if(($count != $i && $rate['from'] <= $additional_price && $additional_price < $rate['to']) || ($count == $i && $rate['from'] <= $additional_price)) {
                                        $additional_price += $_REQUEST['price'] * $rate['btl'];

                                        break;
                                    }
                                    $i++;
                                }
                            }
                            $price            = number_format((float)$price + $additional_price, 2, '.', '');
                            $additional_price = number_format((float)$additional_price, 2, '.', '');
                            $rate             = number_format((float)($price / $_REQUEST['price'] - 1) * 100, 2, '.', '');
                            ?>
                            <div class="screen-2">
                                <p>
                                    Stamp duty land tax (SDLT) has applied since 1 December 2003 when it replaced stamp
                                    duty on Scotland land and buildings. SDLT Is charged in bands which Increase as the
                                    value of the property Increases.
                                </p>
                                <p>
                                    SDLT was restructured on purchases of residential property with effect from 4
                                    December 2014 so that the charge Is now made at different rates depending on the
                                    portion of the purchase price that falls within each rate band. SDLT on
                                    non-residential property is still calculated as a 'slab tax' whereby a single rate.
                                    determined by the value, is applicable to the whole amount.
                                </p>
                                <div class="row">
                                    <div class="col-12 col-md">
                                        <div class="row">
                                            <div class="col-8 offset-2">
                                                <h4>Property Price:</h4>
                                                <input type="text" value="£<?php echo $price; ?>">
                                                <div class="box">
                                                    <h3 class="h4 mb-3">Land and Buildings Transaction Tax to pay is
                                                        £<?php echo $additional_price; ?></h3>
                                                    The effective tax rate is <?php echo $rate; ?>%
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-12 col-md">
                                        <p>
                                            You may have to pay Land and Buildings Transaction Tax (LBTT) on residential
                                            and non-residential land and property transactions. It is the responsibility
                                            of the taxpayer to complete and submit an accurate LBTT return, where
                                            required, and pay any tax due. Further information can be found on:
                                        </p>
                                        <a href="/en/articles/land-and-buildings-transaction-tax-everything-you-need-to-know"
                                           target="_blank">
                                            Land and Buildings Transaction Tax (LBTT) - Everything you need to know
                                        </a>
                                        <h3>Find out more:</h3>
                                        <ul>
                                            <li>
                                                <a class="upfront-home-buying-costs_popup_link" href="#">
                                                    Upfront home buying costs
                                                </a>
                                            </li>
                                            <li>
                                                <a class="mortgage-fees-and-costs_popup_link" href="#">
                                                    Mortgage fees and costs
                                                </a>
                                            </li>
                                            <li>
                                                <a class="costs-of-moving-day_popup_link" href="#">
                                                    Costs of moving day
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="col-12" style="margin-top: 100px">
                        <p>
                            “These Land & Buildings Transaction Tax (“LBTT”), Stamp Duty Land Tax (“SDLT”) and Land Transaction Tax (LTT) Residential Calculators are all estimates and CANNOT be relied on.  There are very specific rules around the purchase of residential land and property so to ensure that you know the exact amount of tax you will need to pay please contact your solicitor.  If you do not have a solicitor please click here and Pacitti Jones can provide a quote for you.
                        </p>
                        <p>
                            We can also advise you on the amount of relevant tax that you will need to pay in relation to an acquisition of commercial land or property. If you need advise on a commercial acquisition please click
                            <a href="">here</a>
                        </p>
                    </div>
                </div>
                <?php do_action('us_after_page'); ?>
            </section>
        </main>

        <?php us_load_template('templates/sidebar') ?>

    </div>
</div>

<?php get_footer() ?>
