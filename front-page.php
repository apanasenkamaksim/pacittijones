<?php get_header(); ?>

<?php //echo do_shortcode('[rev_slider alias="home"]'); ?>

    <section class="hero">
        <img src="<?php echo get_stylesheet_directory_uri() . '/production/images/hero_section.jpg'; ?>" alt="">
        <div class="hover">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <div class="h1 m-0">Find your next home</div>
                        <div class="h4">I'm looking for...</div>
                        <form action="<?php echo home_url(); ?>/search-results/">
                            <div class="row mb-5">
                                <div class="col-6">
                                    <input type="text" name="address_keyword" placeholder="Location">
                                </div>
                                <div class="col-6">
                                    <input type="submit" value="Search">
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="hover-btn">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <a class="pink_btn" href="<?php echo home_url(); ?>/renting/free-rental-valuation/">
                            REQUEST A FREE VALUATION
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </section>

<?php include __DIR__ . '/templates/search-form.php'; ?>

    <section class="welcome">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <h2 class="title"><strong>Welcome to Pacitti Jones.</strong></h2>
                    <p>Pacitti Jones combines the property expertise of an established and successful estate agent with
                        the integrity and professional standards of a law firm to deliver an exceptional property
                        service.</p>
                    <p>We are a full-service estate and letting agency and are a major force in the competitive west of
                        Scotland property market.</p>
                    <p>Unlike most estate agents, however, we are also a well-respected law firm with particular
                        expertise in property law, Wills, executry and matrimonial law. We are regulated by the Law
                        Society of Scotland and are duty bound to act in our client’s best interests at all times</p>
                    <p>Whether you are selling, buying, renting or investing, you can expect from us in-depth market
                        knowledge, professionalism and outstanding service.</p>
                    </p>
                </div>
                <div class="col-md-6">
                    <?php echo do_shortcode('[us_gmaps marker_address="55.818669,-4.2047817" marker_text="" markers="%5B%7B%22marker_address%22%3A%2255.9052084%2C-4.2268099%22%2C%22marker_text%22%3A%22%3Ch6%3EBishopbriggs%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%2C%7B%22marker_address%22%3A%2255.818669%2C-4.2047817%22%2C%22marker_text%22%3A%22%3Ch6%3EBurnside%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%2C%7B%22marker_address%22%3A%2255.7646372%2C-4.1770064%22%2C%22marker_text%22%3A%22%3Ch6%3EEast%20Kilbride%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%2C%7B%22marker_address%22%3A%2255.8636383%2C-4.2584018%22%2C%22marker_text%22%3A%22%3Ch6%3EGlasgow%20City%20Centre%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%2C%7B%22marker_address%22%3A%2255.8733495%2C-4.2993229%22%2C%22marker_text%22%3A%22%3Ch6%3EGlasgow%20West%20End%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%2C%7B%22marker_address%22%3A%2255.9220535%2C-4.1566479%22%2C%22marker_text%22%3A%22%3Ch6%3ELenzie%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%2C%7B%22marker_address%22%3A%2255.8262921%2C-4.2880151%22%2C%22marker_text%22%3A%22%3Ch6%3EShawlands%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%2C%7B%22marker_address%22%3A%2256.1158139%2C-3.9393545%22%2C%22marker_text%22%3A%22%3Ch6%3EStirling%3C%2Fh6%3E%3Cp%3EWe%20will%20be%20glad%20to%20see%20you%20in%20our%20office.%3C%2Fp%3E%22%2C%22marker_size%22%3A%2230%22%7D%5D" height="330" zoom="10" hide_controls="1"]'); ?>
                </div>
            </div>
        </div>
    </section>

    <section class="service">
        <div style="background-image: url('<?php echo get_stylesheet_directory_uri() . '/production/images/service_section.jpg'; ?>')"
             class="image"></div>
        <div class="overflow"></div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4 item">
                    <a class="head" href="<?php echo home_url('/buying'); ?>">
                        <div class="icon">
                            <i class="fa fa-search"></i>
                        </div>
                        <h4 class="title">Buying</h4>
                    </a>
                    <div>If you are interested in purchasing a property, we can help you through every step of the
                        journey.
                    </div>
                </div>
                <div class="col-md-4 item">
                    <a class="head" href="<?php echo home_url('/selling'); ?>">
                        <div class="icon">
                            <i class="fa fa-tag"></i>
                        </div>
                        <h4 class="title">Selling</h4>
                    </a>
                    <div>Our professional property consultants work hard to get you the best possible offer.</div>
                </div>
                <div class="col-md-4 item">
                    <a class="head" href="<?php echo home_url('/renting'); ?>">
                        <div class="icon">
                            <i class="fa fa-home"></i>
                        </div>
                        <h4 class="title">Renting</h4>
                    </a>
                    <div>Smart Move’s team of property experts make finding the perfect property quick and easy.</div>
                </div>
            </div>
        </div>
    </section>

    <section class="cta">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-6">
                    <h2 class="title">Start your journey...</h2>
                    <div>Get a free consultation to discuss your requirements, or arrange a free valuation!</div>
                </div>
                <div class="col-12 col-md-7 col-lg-6">
                    <?php echo do_shortcode('[us_btn link="url:' . urlencode('https://www.pacittijones.scot/free-property-valuation/') . '" label="GET A FREE VALUATION" style="1" color="contrast"][/us_btn]'); ?>
                    <?php echo do_shortcode('[us_btn link="url:' . urlencode('https://www.pacittijones.scot/contact/') . '" label="GET IN TOUCH" style="2" color="white"][/us_btn]'); ?>
                </div>
            </div>
        </div>
    </section>

    <section class="featured_properties">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <h2 class="title">Featured Properties</h2>
                </div>
                <div class="col-12">
                    <?php echo do_shortcode('[featured_properties per_page="6"]'); ?>
                </div>
            </div>
        </div>
    </section>

<?php get_footer(); ?>