<?php defined( 'ABSPATH' ) OR die( 'This script cannot be accessed directly.' );

/**
 * Template to show single page or any post type
 */

$us_layout = US_Layout::instance();

get_header();

global $us_iframe;
if ( ! $us_iframe ) {
	us_load_template( 'templates/titlebar' );
}

?>
<div class="l-main">
	<div class="l-main-h i-cf">

		<main class="l-content"<?php echo ( us_get_option( 'schema_markup' ) ) ? ' itemprop="mainContentOfPage"' : ''; ?>>
            <section class="l-section-h i-cf">
                <?php do_action( 'us_before_page' ); ?>
                <div class="row stamp-duty-calculator">
                    <div class="col-12">
                        <h2 class="h1 font-weight-light mb-4">
                            What our customers say
                        </h2>

                        <p>We always strive to deliver outstanding service and we measure our success by what our clients say about us.</p>
                        <p>All of these client comments are taken from Allagents.co.uk – the independent review website.</p>

                        <?php
                        global $wpdb;
                        $table_name = $wpdb->prefix . 'wpfb_reviews';
                        $sql        = "SELECT * FROM " . $table_name . "
                            WHERE id > 0 AND review_length >= 0 AND type = 'Google'
                            ORDER BY created_time_stamp DESC";
                        $reviews    = $wpdb->get_results($sql);

//                                        var_dump($reviews);
                        ?>
                        
                        <div class="row testimonials">
                            <?php foreach ($reviews as $review) : ?>
                                <a class="col-12" href="<?php echo $review->from_url; ?>" target="_blank">
                                    <div class="rating">
                                        <?php for ($i = 1; $i <= $review->rating; $i++) : ?>
                                            <i class="fas fa-star"></i>
                                        <?php endfor; ?>
                                        <?php for ($i = $review->rating + 1; $i <= $review->rating; $i++) : ?>
                                            <i class="far fa-star"></i>
                                        <?php endfor; ?>
                                    </div>
                                    <div class="name">
                                        <?php echo $review->reviewer_name; ?>
                                    </div>
                                    <div class="text">
                                        <?php echo $review->review_text; ?>
                                    </div>
                                    <div class="date">
                                        <?php
                                        $dt = DateTime::createFromFormat('Y-m-d H:i:s', $review->created_time);;
                                        $dt = $dt->format('d/m/Y');
                                        echo $dt;
                                        ?>
                                    </div>
                                </a>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php do_action( 'us_after_page' ); ?>
            </section>
		</main>

		<?php us_load_template( 'templates/sidebar' ) ?>

	</div>
</div>

<?php get_footer() ?>
