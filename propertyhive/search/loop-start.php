<?php
/**
 * Property Search Results Loop Start
 *
 * @author 		PropertyHive
 * @package 	PropertyHive/Templates
 * @version     1.0.0
 */
?>
<ul class="stl-properties view-<?php echo isset($_GET['view']) && $_GET['view'] != '' ? $_GET['view'] : 'grid'; ?>">