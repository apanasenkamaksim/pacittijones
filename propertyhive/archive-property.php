<?php
/**
 * The Template for displaying property archives, also referred to as 'Search Results'
 *
 * Override this template by copying it to yourtheme/propertyhive/archive-property.php
 *
 * @author      PropertyHive
 * @package     PropertyHive/Templates
 * @version     1.0.0
 */

if(!defined('ABSPATH'))
    exit; // Exit if accessed directly

get_header('propertyhive');
global $wpdb; ?>

    <?php include __DIR__ . '/../templates/search-form.php'; ?>

    <div class="container-fluid">
        <div class="row">
            <div class="col-12">

                <div class="row mb-4 justify-content-between">
                    <div class="col-12 col-lg-auto">
                        <?php propertyhive_catalog_ordering(); ?>

                    </div>
                    <div class="col-12 col-lg-auto">
                        <?php
//                        global $wp_filter;
//                        var_dump($wp_filter['propertyhive_before_search_results_loop']);
//                        remove_action('propertyhive_before_search_results_loop', 'propertyhive_search_form', 10);
//                        remove_action('propertyhive_before_search_results_loop', 'propertyhive_result_count', 20);
//                        remove_action('propertyhive_before_search_results_loop', 'propertyhive_catalog_ordering', 30);
//                        do_action( 'propertyhive_before_search_results_loop' );
                        $map = new PH_Map_Search();
                        $map->propertyhive_results_views();
                        ?>
                    </div>
                </div>

                <?php if ( isset($_GET['view']) && $_GET['view'] == 'map' ) $map->propertyhive_output_map_view(); ?>

                <?php
                // Output results. Filter allows us to not display the results whilst maintaining the main query. True by default
                // Used primarily by the Map Search add on - https://wp-property-hive.com/addons/map-search/
                if(apply_filters('propertyhive_show_results', true)) :
                    ?>

                    <?php if(have_posts()) : ?>

                    <?php propertyhive_property_loop_start(); ?>

                    <?php while (have_posts()) : the_post(); ?>

                        <?php ph_get_template_part('content', 'property'); ?>

                    <?php endwhile; // end of the loop. ?>

                    <?php propertyhive_property_loop_end(); ?>

                <?php else: ?>

                    <?php ph_get_template('search/no-properties-found.php'); ?>

                <?php endif; ?>

                <?php endif; ?>


                <?php
                /**
                 * propertyhive_after_search_results_loop hook
                 *
                 * @hooked propertyhive_pagination - 10
                 */
                do_action('propertyhive_after_search_results_loop');
                ?>

            </div>
        </div>
    </div>

<?php get_footer('propertyhive'); ?>