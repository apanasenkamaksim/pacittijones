<?php
/**
 * The template for displaying a single property within 'Featured Properties' section.
 *
 * Override this template by copying it to yourtheme/propertyhive/content-property-featured.php
 *
 * @author        PropertyHive
 * @package    PropertyHive/Templates
 * @version     1.0.0
 */

if (!defined('ABSPATH')) exit; // Exit if accessed directly

global $property, $propertyhive_loop;

// Store loop count we're currently on
if (empty($propertyhive_loop['loop']))
    $propertyhive_loop['loop'] = 0;

// Store column count for displaying the grid
if (empty($propertyhive_loop['columns']))
    $propertyhive_loop['columns'] = apply_filters('loop_search_results_columns', 1);

// Ensure visibility
if (!$property)
    return;

// Increase loop count
++$propertyhive_loop['loop'];

// Extra post classes
$classes = array('clear');
if (0 == ($propertyhive_loop['loop'] - 1) % $propertyhive_loop['columns'] || 1 == $propertyhive_loop['columns'])
    $classes[] = 'first';
if (0 == $propertyhive_loop['loop'] % $propertyhive_loop['columns'])
    $classes[] = 'last';
if ($property->featured == 'yes')
    $classes[] = 'featured';
?>
<li <?php post_class($classes); ?>>

    <?php do_action('propertyhive_before_search_results_loop_item'); ?>

    <a href="<?php the_permalink(); ?>">
        <div class="thumbnail">
            <?php
            /**
             * propertyhive_before_search_results_loop_item_title hook
             *
             * @hooked propertyhive_template_loop_property_thumbnail - 10
             */
            do_action('propertyhive_before_search_results_loop_item_title');
            ?>
        </div>
        <div class="details">

            <h3 class="title"><?php the_title(); ?></h3>

            <div class="description">
                <?php propertyhive_template_loop_summary(); ?>
            </div>

            <div class="bottom">
                <?php propertyhive_template_loop_price(); ?>

                <div class="bedrooms">
                    <i class="fas fa-bed"></i>
                    <?php echo get_post_meta(get_the_ID(), '_bedrooms', true); ?>
                </div>
            </div>

        </div>
    </a>

    <?php do_action('propertyhive_after_search_results_loop_item'); ?>

</li>