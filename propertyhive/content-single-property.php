<?php
/**
 * The template for displaying property content in the single-property.php template
 *
 * Override this template by copying it to yourtheme/propertyhive/content-single-property.php
 *
 * @author      PropertyHive
 * @package     PropertyHive/Templates
 * @version     1.0.0
 */

if(!defined('ABSPATH'))
    exit; // Exit if accessed directly

global $property;
?>

<?php
if(post_password_required()) {
    echo get_the_password_form();
    return;
}
?>

<div class="property-header">
    <div class="container-fluid">
        <div class="row align-items-center">
            <div class="col-12 col-lg-auto first-col">
                <?php propertyhive_template_single_title(); ?>
                <?php propertyhive_template_single_price(); ?>
            </div>
            <div class="col-12 col-lg"></div>
            <div class="col-12 col-lg-auto">
                <?php echo do_shortcode('[us_breadcrumbs show_current="1" font_size="0.9rem" separator_icon="material|chevron_right" align="right"]'); ?>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div id="content" role="main">
        <div id="property-<?php the_ID(); ?>" <?php post_class(); ?>>

            <div class="row">
                <div class="col-12 col-lg-8 left-col">
                    <?php
                    global $post;
                    $ids = get_post_meta($post->ID, '_photos', true);
                    ?>
                    <?php echo do_shortcode('[gallery ids="' . implode(',', $ids) . '" royalslider="3"]'); ?>
                    <?php
                    ob_start();
                    propertyhive_template_single_summary();
                    $summary = ob_get_clean();

                    ob_start();
                    propertyhive_template_single_features();
                    $features = ob_get_clean();

                    ob_start();
                    get_property_street_view();
                    $street_view = ob_get_clean();

                    echo do_shortcode('[vc_tta_tabs title_font="body"]
                        [vc_tta_section title="SUMMARY" tab_id="one" icon="material|star_border"]' . $summary . '[/vc_tta_section]
                        [vc_tta_section title="DETAILS" tab_id="two" icon="material|info"]' . $features . '[/vc_tta_section]
                        [vc_tta_section title="MAP" tab_id="three" icon="material|location_on"]
                            [property_map]
                        [/vc_tta_section]
                        [vc_tta_section title="STREET VIEW" tab_id="four"]' . $street_view . '[/vc_tta_section]
                    [/vc_tta_tabs]');
                    ?>
                    <script>
                        jQuery(document).ready(function ($) {
                            $('.w-tabs-item a[href="#four"]').click(function () {
                                var myLatlng = new google.maps.LatLng(<?php echo $property->latitude; ?>, <?php echo $property->longitude; ?>);
                                var streetViewOptions = {
                                    position: myLatlng,
                                    pov: {
                                        heading: 90,
                                        pitch: 0,
                                        zoom: 0
                                    }
                                };

                                <?php do_action('propertyhive_property_street_view_options'); ?>

                                var streetView = new google.maps.StreetViewPanorama(document.getElementById("property_street_view_canvas"), streetViewOptions);
                                streetView.setVisible(true);
                            });
                        });
                    </script>
                </div>
                <div class="col-12 col-lg-4 right-col">
                    <div>
                        <div class="availability">
                            <?php echo $property->availability; ?>
                        </div>
                    </div>

                    <h4>Call:&nbsp;<strong>020 7698 4433</strong></h4>

                    <?php propertyhive_make_enquiry_button(); ?>

                    <div class="w-separator type_default size_small thick_1 style_dashed color_border cont_none"><span
                                class="w-separator-h"></span></div>

                    <div class="overview">
                        <h4 class="title">
                            Overview
                        </h4>
                        <?php
                        /**
                         * propertyhive_single_property_summary hook
                         *
                         * @hooked propertyhive_template_single_title - 5
                         * @hooked propertyhive_template_single_floor_area - 7
                         * @hooked propertyhive_template_single_price - 10
                         * @hooked propertyhive_template_single_meta - 20
                         * @hooked propertyhive_template_single_sharing - 30
                         */
                        remove_action('propertyhive_single_property_summary', 'propertyhive_template_single_title', 5);
                        remove_action('propertyhive_single_property_summary', 'propertyhive_template_single_floor_area', 7);
                        remove_action('propertyhive_single_property_summary', 'propertyhive_template_single_price', 10);
                        add_filter('propertyhive_single_property_meta', function($meta) {
                            unset($meta['availability']);
                            return $meta;
                        });
                        global $wp_filter;
                        do_action('propertyhive_single_property_summary');
                        ?>
                    </div>

                    <?php
                    ob_start();
                    remove_action('propertyhive_property_actions_list_start', 'propertyhive_make_enquiry_button', 10);
                    propertyhive_template_single_actions();
                    $actions = ob_get_clean();
                    ?>
                    <?php if(strip_tags($actions) != $actions) : ?>
                    <div class="w-separator type_default size_small thick_1 style_dashed color_border cont_none"><span
                                class="w-separator-h"></span></div>

                    <div class="actions">
                        <h4 class="title">
                            Actions
                        </h4>
                        <?php echo $actions; ?>
                    </div>
                    <?php endif; ?>

                    <div class="w-separator type_default size_small thick_1 style_dashed color_border cont_none"><span
                                class="w-separator-h"></span></div>

                    <div class="newtworks">
                        <?php echo do_shortcode('[us_sharing align="left" type="solid" providers="facebook,twitter,linkedin" shape="circle" size="24px" gap="0.1em"]'); ?>
                    </div>

                </div>

                <?php //get_property_street_view(); ?>

            </div><!-- #property-<?php the_ID(); ?> -->
        </div>
    </div>