<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 7/13/2019
 * Time: 6:44 PM
 */
?>

<section class="search_form">
    <div class="container-fluid">
        <div class="row m-0">
            <div class="col-12">
                <?php
                echo do_shortcode('[vc_tta_tabs title_font="body"]
                    [vc_tta_section title="FOR SALE" tab_id="one"]
                        [property_search_form id="for_sale"]
                    [/vc_tta_section]
                    [vc_tta_section title="TO RENT" tab_id="two"]
                        [property_search_form id="to_rent"]
                    [/vc_tta_section]
                [/vc_tta_tabs]');
                ?>
            </div>
        </div>
    </div>
</section>