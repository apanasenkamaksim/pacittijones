<?php
function get_popups() {
    /**
     * Adding popups.
     */

    if(is_page('stamp-duty-calculator')) {
        require __DIR__ . '/popups/upfront-home-buying-costs.php';
        require __DIR__ . '/popups/mortgage-fees-and-costs.php';
        require __DIR__ . '/popups/costs-of-moving-day.php';

    }
}
add_action('wp_footer', 'get_popups');