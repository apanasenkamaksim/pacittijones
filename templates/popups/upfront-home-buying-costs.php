<div class="upfront-home-buying-costs_popup fancybox-content" style="display: none;">
    <div class="flex">
        <h2 class="h1 title">
            The cost of buying a house and moving
        </h2>
        <div class="body">
            <p>
                Wondering how much it costs to buy a house or a flat? There are a number of fees to consider when buying a new house, including the cost of moving home, your deposit and solicitor fees. This guide will help you plan for all of these when you work out your budget.
            </p>
            <p>
                <b>
                    Major upfront costs
                </b>
            </p>
            <p>
                Make sure you have saved enough to cover all of the upfront costs.
            </p>
            <p>
                These include:
            </p>
            <p>
                <b>
                    Stamp duty
                </b>
            </p>
            <p>
                Stamp duty is a government tax paid on homes costing £125,001 or more.
            </p>
            <p>
                First-time-buyers will pay no Stamp Duty on the first £300,000 for properties worth up to £500,000.
            </p>
            <p>
                As of April 2016, there is a 3% increase on top of current rates if you’re buying an additional residential property above £40,000 such as a second home or buy-to-let property.
            </p>
            <p>
                <b>
                    Deposit
                </b>
            </p>
            <p>
                This is the amount you put towards the cost of the property when you buy your home.
            </p>
            <p>
                On average, you need at least 5% to 20% of the purchase price (for example: £10,000 to £ 40,000 when buying a £200,000 home).
            </p>
            <p>
                <b>
                    Valuation fee
                </b>
            </p>
            <p>
                The mortgage lender will assess the value of the property to establish how much they are prepared to lend you.
            </p>
            <p>
                The cost can be £150-£1,500 based on the property’s value.
            </p>
            <p>
                Some lenders might not charge you for this, depending on the type of mortgage product you select.
            </p>
            <p>
                The lender’s valuation is not like a full structural survey so it might not identify all the repairs or maintenance that might be needed.
            </p>
            <p>
                <b>
                    Surveyor’s fee
                </b>
            </p>
            <p>
                Before you buy a property, get it checked by a surveyor.
            </p>
            <p>
                This is vital so you understand if there are any problems before you buy.
            </p>
            <p>
                Surveys range from a basic home condition survey costing around £250 to a full structural survey from £600 or more.
            </p>
            <p>
                Paying for a good survey could save you money on repairs in the long run.
            </p>
            <p>
                <b>
                    Legal fees
                </b>
            </p>
            <p>
                You’ll normally need a solicitor or licensed conveyor to carry out all the legal work when buying and selling your home.
            </p>
            <p>
                Legal fees are typically £850-£1,500 including VAT at 20%.
            </p>
            <p>
                They will also do local searches, which will cost you £250-£300, to check whether there are any local plans or problems.
            </p>
            <p>
                <b>
                    Electronic transfer fee
                </b>
            </p>
            <p>
                Typically this costs £40-£50.
            </p>
            <P>
                It covers the lenders cost of transferring the mortgage money from the lender to the solicitor.
            </P>
            <p>
                <b>
                    Estate agent’s fee
                </b>
            </p>
            <p>
                This is only paid by the seller, not the buyer, for the estate agent’s services.
            </p>
            <p>
                It is negotiated when they put the property on the market.
            </p>
            <p>
                It is usually 1% to 3% of the sale price plus 20% VAT.
            </p>
            <p>
                <b>
                    Removal costs
                </b>
            </p>
            <p>
                These usually range from £300-£600, although you could rent a van do it yourself.
            </p>
            <p>
                <b>
                    Mortgage costs
                </b>
            </p>
            <p>
                We’ve put together a home buying timeline to help give you an idea of the costs you can expect throughout the process.
            </p>
            <p>
                <b>
                    Mortgage fees
                </b>
            </p>
            <p>
                These might include:
            </p>
            <p>
                A booking fee of £99-£250
            </p>
            <p>
                An arrangement fee of up to £2,000, and
            </p>
            <p>
                A mortgage valuation fee (£150 or more).
            </p>
            <p>
                It’s best to pay these upfront rather than adding them to your mortgage, otherwise you’ll be paying interest on them for the life of the mortgage.
            </p>
            <p>
                <b>
                    Types of mortgage
                </b>
            </p>
            <p>
                There are hundreds of types of mortgage products and several types of mortgage for different circumstances.
            </p>
            <p>
                Look beyond the interest rate. Make sure you also take the fees and charges into account when choosing a mortgage.
            </p>
            <p>
                <b>
                    Ongoing costs
                </b>
            </p>
            <p>
                Remember once you buy your own home you’re responsible for looking after it.
            </p>
            <p>
                <b>
                    Maintenance and repairs
                </b>
            </p>
            <p>
                The average repair bill for new homeowners is £5,750.
            </p>
            <p>
                Your survey should have highlighted any problems that need fixing straight away.
            </p>
            <p>
                <b>
                    Insurance
                </b>
            </p>
            <p>
                The lender will require you to take out buildings insurance to protect your new home against damage from fire, floods, subsidence and anything else.
            </p>
            <p>
                It’s also a good idea to have contents insurance for all your possessions, and life insurance to pay off your mortgage should you die before you’ve repaid the entire amount.
            </p>
            <p>
                <b>
                    Council Tax
                </b>
            </p>
            <p>
                The amount you pay is based on where the property is and its valuation band the property is in (apart from in Northern Ireland where rates are set individually).
            </p>
            <p>
                <b>
                    Running costs
                </b>
            </p>
            <p>
                Ask the sellers how much they spend on utilities – gas, electricity and water – every year.
            </p>
            <p>
                Don’t forget to think about charges for your phone, TV packages and broadband.
            </p>
            <p>
                <b>
                    Leaseholders’ costs
                </b>
            </p>
            <p>
                If you buy a leasehold property, you’ll have to pay ground rent (around £50-£100 a year) and service charges to the person who owns the freehold.
            </p>
            <p>
                Service charges and admin fees differ between properties.
            </p>
            <p>
                This is an important cost of running a property so it’s vital you know more about these charges.
            </p>
            <p>
                <b>
                    Freehold or leasehold
                </b>
            </p>
            <p>
                There are different ways you can own property: freehold, leasehold or through a share of the freehold.
            </p>
            <p>
                <b>
                    Costs of moving home
                </b>
            </p>
            <p>
                Once you know the cost of your new mortgage payments, new insurance policies, solicitor and estate agent fees, make time to find out about moving-day costs.
            </p>
            <p>
                Whether you’re a first-time buyer or a seasoned home mover, it’s important to know and prepare for these costs, which can be surprisingly high.
            </p>
            <p>
                <b>
                    House moving checklist
                </b>
            </p>
            <p>
                Our checklist make sure you’re ready to move home :
            </p>
            <p>
                <b>
                    House removal costs
                </b>
            </p>
            <p>
                The average removal cost range from £300-£600, although you could rent a van do it yourself.
            </p>
            <p>
                Remember to shop around for quotes (and references) to find a reliable firm.
            </p>
            <p>
                <b>
                    House removal insurance
                </b>
            </p>
            <p>
                Check the removal firm you hire is insured.
            </p>
            <p>
                If you’re moving yourself, think about arranging insurance cover. Check if your existing home insurance policy will cover your move – many policies will if you’re using a professional removal firm.
            </p>
            <p>
                <b>
                    Storage costs
                </b>
            </p>
            <p>
                Shop around to compare prices and security arrangements, and get a sense of average costs. Estimate the length of time you’ll need storage, because prices will depend on this.
            </p>
            <p>
                <b>
                    Cleaning costs
                </b>
            </p>
            <p>
                If you’re moving from a rental property you must usually leave it clean and tidy. Not doing so might contravene your tenancy agreement and your landlord might be allowed to charge you for the cost of a professional cleaner.
            </p>
            <p>
                You might want to shop around and pay a professional to clean your property before you leave.
            </p>
            <p>
                <b>
                    Extra moving-day costs
                </b>
            </p>
            <p>
                Do you need to budget for extra childcare or kennels for a pet? Will you have the time or facilities to cook or should you plan for takeaways for a day or two?
            </p>
            <p>
                These costs can all add up so make sure you include them in your budget.
            </p>
        </div>
    </div>
</div>