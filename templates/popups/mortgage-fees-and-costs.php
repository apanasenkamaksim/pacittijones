<div class="mortgage-fees-and-costs_popup fancybox-content" style="display: none;">
    <div class="flex">
        <h2 class="h1 title">
            A guide to mortgage fees and costs
        </h2>
        <div class="body">
            <p>
                <b>
                    There are a number of fees and charges you might need to pay if you’re taking out a mortgage. These include mortgage broker fees, adviser fees, valuation fees, arrangement fees and more. Use our handy mortgage costs table to find out how they all work and how much you might have to pay.
                </b>
            </p>
            <p>
                <b>
                    Fees and charges
                </b>
            </p>
            <p>
                Wondering how much mortgage fees will cost you? It can depend on a number of factors, like your personal situation, or the mortgage product you’re applying for.
            </p>
            <p>
                The table below will give you an idea of what to expect.
            </p>
            <p>
                Lenders can use different terms to describe their fees, so make sure you know what each cost includes and when you’ll need to pay.
            </p>
            <p>
                <b>
                    Mortgage costs
                </b>
            </p>
            <p>
                Since March 2016, mortgage lenders have to include any mortgage related fees, such as redemption charges and valuation fees, as part of the annual interest calculation. This way of calculating the interest is called the Annual Percentage Rate of Charge or APRC.
            </p>
            <p>
                All mortgage product related costs should be outlined in a mortgage Illustration document. It is sometimes called a European Standard Information Sheet (ESIS), or an enhanced keyfacts Illustration with supplements of any required additional information as needed.
            </p>
            <p>
                <b>
                    Other mortgage-related charges
                </b>
            </p>
            <p>
                Mortgage-related charges can add thousands of pounds to your costs.
            </p>
            <p>
                These include:
            </p>
            <p>
                Moving costs
            </p>
            <p>
                Legal and survey fees
            </p>
            <p>
                Stamp Duty on residential property purchases above £125,000
            </p>
            <p>
                First-time-buyers will pay no Stamp Duty on the first £300,000 for properties worth up to £500,000.
            </p>
            <p>
                If you’re purchasing an additional home, for example a buy-to-let property for more than £40,000, you’ll have to pay an extra 3% on top of each Stamp Duty band.
            </p>
            <p>
                Picking the right mortgage deal
            </p>
            <p>
                Buying a property is a big investment and it is a good idea to get some advice.
            </p>
            <p>
                Some mortgage deals might seem attractive, but fees can quickly add up.
            </p>
            <p>
                When comparing mortgage offers, add up all the charges over the length of the deal as well as your monthly repayments.
            </p>
            <p>
                For example, if your repayments are £1,000 per month on a two-year fixed-rate mortgage, plus £300 in fees, the total cost of the deal is £24,300.
            </p>
        </div>
    </div>
</div>