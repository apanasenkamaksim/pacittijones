<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 9/10/2019
 * Time: 5:23 PM
 */

function meet_our_team_func( $atts ){
    $categories = get_categories( array(
        'taxonomy'     => 'branch',
        'orderby'      => 'menu_order',
        'order'        => 'ASC',
        'hide_empty'   => 0,
    ) );

    ob_start();
    ?>

    <div class="row meet_out_team">
        <?php foreach($categories as $cat) : ?>
        <div class="col-12 col-lg-4 branch">
            <h3 class="title">
                <?php echo $cat->name; ?>
            </h3>
            <?php $image = get_field('image', 'term_' . $cat->term_id); ?>
            <div>
                <img src="<?php echo $image['sizes']['medium']; ?>" alt="">
            </div>
            <a href="<?php echo get_term_link($cat->term_id); ?>" class="btn">
                Meet Our Team
            </a>
        </div>
        <?php endforeach; ?>
    </div>

    <?php
    return ob_get_clean();
}
add_shortcode('meet_our_team', 'meet_our_team_func');