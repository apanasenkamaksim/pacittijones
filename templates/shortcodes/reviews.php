<?php
/**
 * Created by seattleby.com
 * User: Maksim Apanasenka
 * Date: 9/10/2019
 * Time: 5:23 PM
 */

function reviews_func($atts) {
    global $wpdb;
    $table_name = $wpdb->prefix . 'wpfb_reviews';
    $sql        = "SELECT * FROM " . $table_name . "
				WHERE id > 0 AND review_length >= 0 AND type = 'Google'
				ORDER BY created_time_stamp DESC
				LIMIT 3";
    $reviews    = $wpdb->get_results($sql);

    //                var_dump($reviews);

    ob_start();
    ?>

    <section class="reviews">
        <div class="container-fluid">
            <div class="row">
                <?php foreach ($reviews as $review) : ?>
                    <a class="col-12 col-md-4" href="<?php echo home_url('/testimonials/'); ?>">
                        <div class="rating">
                            <?php for ($i = 1; $i <= $review->rating; $i++) : ?>
                                <i class="fas fa-star"></i>
                            <?php endfor; ?>
                            <?php for ($i = $review->rating + 1; $i <= $review->rating; $i++) : ?>
                                <i class="far fa-star"></i>
                            <?php endfor; ?>
                        </div>
                        <div class="name">
                            <?php //echo $review->reviewer_name; ?>
                        </div>
                        <div class="text">
                            <?php echo $review->review_text; ?>
                        </div>
                        <div class="date">
                            <?php
                            $dt = DateTime::createFromFormat('Y-m-d H:i:s', $review->created_time);;
                            $dt = $dt->format('d/m/Y');
                            echo $dt;
                            ?>
                        </div>
                    </a>
                <?php endforeach; ?>
            </div>
        </div>
    </section>

    <?php
    return ob_get_clean();
}

add_shortcode('reviews', 'reviews_func');