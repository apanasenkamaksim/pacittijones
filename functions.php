<?php
/**
 * Enqueue scripts and styles.
 */
require_once __DIR__ . '/inc/enqueue_scripts_styles.php';
require_once __DIR__ . '/inc/cpt.php';
require_once __DIR__ . '/templates/shortcodes.php';
require_once __DIR__ . '/templates/popups.php';
//require_once __DIR__ . '/inc/functions.php';

function standard_gallery() {
    if(shortcode_exists('gallery')) {
        remove_shortcode('gallery');
    }
    add_shortcode('gallery', 'gallery_shortcode');
}

add_action('init', 'standard_gallery', 30);

remove_action('propertyhive_before_main_content', 'propertyhive_output_content_wrapper', 10);
remove_action('propertyhive_after_main_content', 'propertyhive_output_content_wrapper_end', 10);


function add_padding_below_header() {
    ?>
    <div class="l-section"></div>
    <?php
}

add_action('us_after_header', 'add_padding_below_header');

function add_additional_view($views) {
    $views = array();
    $views['grid'] = array('default' => true, 'content' => '<i class="fas fa-th"></i>');
    $views['list'] = array('content' => '<i class="fas fa-th-list"></i>');
    $views['map'] = array('content' => '<i class="fas fa-map-marker-alt"></i>');

    return $views;
}

add_filter('propertyhive_results_views', 'add_additional_view', 1);
