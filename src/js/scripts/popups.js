jQuery(document).ready(function ($) {

    $('.close_popups_btn').click(function () {
        $.fancybox.close(true);
    });

    $('.upfront-home-buying-costs_popup_link').click(function () {
        $.fancybox.close(true);
        $.fancybox.open({
            src: '.upfront-home-buying-costs_popup',
            type: 'inline'
        });

        return false;
    });
    $('.mortgage-fees-and-costs_popup_link').click(function () {
        $.fancybox.close(true);
        $.fancybox.open({
            src: '.mortgage-fees-and-costs_popup',
            type: 'inline'
        });

        return false;
    });
    $('.costs-of-moving-day_popup_link').click(function () {
        $.fancybox.close(true);
        $.fancybox.open({
            src: '.costs-of-moving-day_popup',
            type: 'inline'
        });

        return false;
    });
});


