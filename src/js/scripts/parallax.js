// The plugin code
window.viewport = function () {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {width: e[a + 'Width'], height: e[a + 'Height']};
};

jQuery(document).ready(function($) {
    function transformparallax(element, options) {
        this.element = $(element);
        this.opts = options;
    }

    let methods = {
        init : function(element, options) {
            if(element.length < 1) return;
            let parent = element.parent();
            let offset = parent.offset();
            let defaults = {
                'pozX' : 0,
                'start': offset.top,
                'stop': offset.top + parent.outerHeight(),
                'coeff': 0.4
            };
            let opts = $.extend(defaults, options);

            element.css({
                'height' : (window.viewport().height + parent.outerHeight()) * opts.coeff + parent.outerHeight()
            });

            let data = new transformparallax(element, opts);
            element.data('transformparallax', data);
            element.addClass('transformparallax');
            methods.parallax(element, opts);
        },
        parallax : function (element, opts) {
            let windowTop = $(window).scrollTop();
            if((windowTop + window.viewport().height >= opts.start) && (windowTop <= opts.stop)) {
                let newCoord = (windowTop + window.viewport().height - opts.start) * opts.coeff;
                element.css({
                    'transform': 'translateX(' + opts.pozX + ') translateY(-' + newCoord + 'px)'
                });
            }
        }
    };

    $(window).scroll(function () {
        $('.transformparallax').each(function () {
            let data = $(this).data('transformparallax');
            if(data) {
                methods.parallax(data.element, data.opts);
            }
        });
    });

    $.fn.transformparallax = function(options) {
        return this.each(function() {
            methods.init($(this), options);
        });
    };
});