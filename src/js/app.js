import $ from 'jquery';
import Popper from 'popper.js';

import '@fancyapps/fancybox/dist/jquery.fancybox.min';

//Custom

window.viewport = function () {
    var e = window, a = 'inner';
    if (!('innerWidth' in window)) {
        a = 'client';
        e = document.documentElement || document.body;
    }
    return {width: e[a + 'Width'], height: e[a + 'Height']};
};

import './scripts/parallax';
import './scripts/popups';

jQuery(document).ready(function ($) {
    $('section.service > .image').transformparallax();

    var resizeTimer;
    $(window).on('resize', function (e) {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function () {
            // Run code here, resizing has "stopped"
            $('section.service > .image').transformparallax();
        }, 250);
    });

    window.setInterval(function () {
        if ($('#pc4uCF7Dropdown_Postcode').css('display') == 'none') {
            $('#pc4uCF7Dropdown_Postcode').parent('.type_select').css({display: 'none'});
        } else {
            $('#pc4uCF7Dropdown_Postcode').parent('.type_select').css({display: 'inline-block'});
        }
    }, 100);

    $('.info-block .info-icon, .info-block .info-close').click(function () {
       $(this).closest('.info-block ').find('.info-icon').toggleClass('active');
    });
});